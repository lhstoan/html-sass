// JavaScript Document
$(function () {
	"use strict";
	var obj = {
		init: function () {
			this.toTop();
			this.smoothScroll();
			this.iconMenu();
			this.jsUnder();
		},
		//totop
		toTop: function () {
			$("#totop").hide();
			$(window).scroll(function () {
				if ($(this).width() <= 750) {
					if ($(this).scrollTop() > 400) {
						$("#totop").fadeIn();
					} else {
						$("#totop").fadeOut();
					}
				} else {
					if ($(this).scrollTop() > 100) {
						$("#totop").fadeIn();
					} else {
						$("#totop").fadeOut();
					}
				}
			});
			$("#totop a").click(function () {
				$("body,html").animate(
					{
						scrollTop: 0,
					},
					500
				);
				return false;
			});
		},
		// js under page
		jsUnder: function () {
			//img
			const img = $(".img-l,.img-r");
			if (img) {
				$(img).parent().addClass("clearfix");
			}

			const tblImg = $("table img");
			if (tblImg) {
				$(tblImg).parent().addClass("tbl-img");
			}
		},
		//smoothScroll
		smoothScroll: function () {
			$('a[href^="#"]').click(function () {
				if ($($(this).attr("href")).length) {
					let pos = $($(this).attr("href")).offset().top;
					loadAnchor(pos);
				}
			});

			$(window).load(function () {
				let hash = location.hash;
				if (hash !== "") {
					let pos = $(hash).offset().top;
					loadAnchor(pos);
				}
			});

			// custom anchor for one page
			const urlOnePage = $("");
			$(urlOnePage).click(function () {
				$(".hamburger, .gnavi").removeClass("active");
				$("body").removeClass("block");

				let arrHref = $(this).attr("href").split("#");
				let hash = "#" + arrHref[arrHref.length - 1];
				if (hash !== "") {
					let pos = $(hash).offset().top;
					loadAnchor(pos);
				}
			});

			function loadAnchor(pos) {
				let $root = $("html, body");
				if ($(window).width() > 750) {
					$root.animate(
						{
							scrollTop: pos - 155,
						},
						600
					);
				} else {
					$root.animate(
						{
							scrollTop: pos - 100,
						},
						600
					);
				}
			}
		},
		fixed: function () {
			$(".fixed-contact").removeClass("active");
			$(window).on("scroll load resize", function () {
				if ($(this).scrollTop() > 200) {
					$("header").addClass("scroll");
				} else {
					$("header").removeClass("scroll");
				}
				if ($(this).width() <= 750) {
					if ($(this).scrollTop() > 400) {
						$(".fixed-contact").addClass("active");
					} else {
						$(".fixed-contact").removeClass("active");
					}
				}
			});
		},
		//sp gnavi
		iconMenu: function () {
			$(".hicon").click(function () {
				$("body").toggleClass("open-nav");
				if ($("body").hasClass("open-nav")) {
				} else {
					$(".hmenu-lv2").slideUp();
					$(".gnavi .has").removeClass("active");
				}
			});

			$(".gnavi .has").click(function () {
				const ww = $(window).width();
				if (ww <= 750) {
					$(this).toggleClass("active");
					$(this).find(".hmenu-lv2").slideToggle();
				}
			});

			$(window).on("load resize", function () {
				const ww = $(window).width();
				if (ww > 750) {
					$("body").removeClass("open-nav");
					$(".gnavi .has").removeClass("active");
				}
			});
		},
	};

	obj.init();
});
