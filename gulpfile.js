const gulp = require("gulp");
const fileinclude = require("gulp-file-include");
const server = require("browser-sync").create();
const { watch, series } = require("gulp");
var gimagemin = require("gulp-imagemin");

const sass = require("gulp-sass");
sass.compiler = require("node-sass");

// Reload Server
async function reload() {
	server.reload();
}

// Sass compiler
async function compileSass() {
	gulp.src("./src/sass/**/*.scss")
		.pipe(sass().on("error", sass.logError))
		.pipe(gulp.dest("./src/css"));
}
// // Sass compiler
async function OptimizeImg() {
	gulp.src("./src/images/*").pipe(gimagemin()).pipe(gulp.dest("html/images"));
}

// Copy assets after build
async function copyAssets() {
	gulp.src(["src/**/*"]).pipe(gulp.dest("./html"));
}

// Build files html and reload server
async function buildAndReload() {
	await includeHTML();
	await OptimizeImg();
	await copyAssets();
	reload();
}

async function includeHTML() {
	return gulp
		.src(["./src/**/*.html", "!./src/template/**"])
		.pipe(
			fileinclude({
				prefix: "@@",
				basepath: "@file",
			})
		)
		.pipe(gulp.dest("./html"));
}
exports.includeHTML = includeHTML;

exports.default = async function () {
	// Init serve files from the build folder
	server.init({
		server: {
			baseDir: "./html",
		},
	});
	// Build and reload at the first time
	buildAndReload();
	// Watch Sass task
	watch("./src/sass/**/*.scss", series(compileSass));
	// Watch task
	watch(["./src/**/*.html", "src/**/*"], series(buildAndReload));
};
